#!/usr/bin/env bash

DOCKERHUB_NAME=apecr

# # Compile and publish Server
SERVER_IMAGE_NAME="${DOCKERHUB_NAME}/server:1.0.0"

printf "\n==> Compile and public Server image with name '%s', using a Dockerfile\n" ${SERVER_IMAGE_NAME}
docker build -t ${SERVER_IMAGE_NAME} ./server
docker push ${SERVER_IMAGE_NAME}

# Compile and publish WeatherService
EXTERNAL_SERVICE_IMAGE_NAME="${DOCKERHUB_NAME}/externalservice:1.0.1"

printf "\n==> Compile and public ExternalService image with name '%s', using BuildPacks\n" ${EXTERNAL_SERVICE_IMAGE_NAME}
pack build ${EXTERNAL_SERVICE_IMAGE_NAME} --path ./externalservice \
    --builder gcr.io/buildpacks/builder:v1

docker push ${EXTERNAL_SERVICE_IMAGE_NAME}

# Compile and publish Worker with multistage Dockerfile
WORKER_MULTISTAGE_IMAGE_NAME="${DOCKERHUB_NAME}/worker:0.0.1-multistage"

printf "\n==> Compile and public Worker image with name '%s', using a Multistage Dockerfile\n" ${WORKER_MULTISTAGE_IMAGE_NAME}
docker build -t ${WORKER_MULTISTAGE_IMAGE_NAME} ./worker
docker push ${WORKER_MULTISTAGE_IMAGE_NAME}

# Compile and publish Worker with JIB
WORKER_JIB_IMAGE_NAME="${DOCKERHUB_NAME}/worker:0.0.1-jib"

printf "\n==> Compile and public Worker image with name '%s', using JIB\n" ${WORKER_JIB_IMAGE_NAME}
mvn -f worker/ install jib:build -Dimage=${WORKER_JIB_IMAGE_NAME}

exit 0